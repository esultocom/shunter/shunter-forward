# Shunter Forward

[![Made by ESULTO](https://img.shields.io/badge/made%20by-ESULTO-007ec6.svg?style=flat-square)](https://esulto.com) [![pipeline status](https://gitlab.com/esultocom/shunter/shunter-forward/badges/main/pipeline.svg)](https://gitlab.com/esultocom/shunter/shunter-forward/-/commits/main) [![coverage report](https://gitlab.com/esultocom/shunter/shunter-forward/badges/main/coverage.svg)](https://gitlab.com/esultocom/shunter/shunter-forward/-/commits/main)

`Shunter Forward` is simple Docker container that forwards traffic to a specific IP.

## License

MIT