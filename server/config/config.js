/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                 *
 *    This file is part of shunter-forward which is released under MIT.                            *
 *    See file 'LICENSE' for full license details.                                                 *
 *                                                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// * * * * * * * * Import externals
const dotenv = require('dotenv');
const path = require('path');
const Joi = require('joi');

// * * * * * * * * The code
dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi
      .string()
      .valid('production', 'development', 'test')
      .default('production'),
    SHUNTER_LOCAL_ADDRESS: Joi
      .string()
      .ip({
        version: [
          'ipv4',
          'ipv6'
        ]
      })
      .default('0.0.0.0'),
    SHUNTER_LOCAL_PORT: Joi
      .number()
      .integer()
      .min(0)
      .max(65535)
      .default(3000),
    SHUNTER_REMOTE_ADDRESS: Joi
      .string()
      .ip({
        version: [
          'ipv4',
          'ipv6'
        ]
      })
      .required(),
    SHUNTER_REMOTE_PORT: Joi
      .number()
      .integer()
      .min(0)
      .max(65535)
      .required()
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

// * * * * * * * * Module exports
module.exports = {
  env: envVars.NODE_ENV,
  local: {
    address: envVars.SHUNTER_LOCAL_ADDRESS,
    port: envVars.SHUNTER_LOCAL_PORT
  },
  remote: {
    address: envVars.SHUNTER_REMOTE_ADDRESS,
    port: envVars.SHUNTER_REMOTE_PORT
  }
};
