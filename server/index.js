/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                 *
 *    This file is part of shunter-forward which is released under MIT.                            *
 *    See file 'LICENSE' for full license details.                                                 *
 *                                                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// * * * * * * * * Import internals
const app = require('./app');
const config = require('./config/config');
const logger = require('./config/logger');
const { name, version } = require('../package.json');

// * * * * * * * * The code
const server = app.listen(config.local.port, config.local.address, () => {
  logger.info(`Server ${name} release ${version}!`);
  logger.info(`Listening on address ${config.local.address} port ${config.local.port}`);
  logger.info(`Forward ${config.protocol} traffic to address ${config.remote.address} port ${config.remote.port}`);
});

const exitHandler = async () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = async (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);
process.on('SIGTERM', exitHandler);
process.on('SIGINT', exitHandler);
