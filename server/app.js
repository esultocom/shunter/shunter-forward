/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                 *
 *    This file is part of shunter-forward which is released under MIT.                            *
 *    See file 'LICENSE' for full license details.                                                 *
 *                                                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// * * * * * * * * Import externals
const net = require('net');

// * * * * * * * * Import internals
const config = require('./config/config');
const logger = require('./config/logger');

// * * * * * * * * The code
var app = net.createServer((inputSocket) => {
  let outputSocket = net.createConnection(config.remote.port, config.remote.address);

  inputSocket.on('error', (err) => {
    logger.error(`Destination is unreachable: ${server}:${port}. ${err && err.message}`)
  });

  inputSocket.pipe(outputSocket);
  outputSocket.pipe(inputSocket);
});

// * * * * * * * * Module exports
module.exports = app;
